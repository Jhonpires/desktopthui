﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
    /// <summary>
    /// Lógica interna para FrmCadCardapio.xaml
    /// </summary>
    public partial class FrmCadCardapio : Window
    {
        string codigoEmp, codigoCat;
        public FrmCadCardapio()
        {
            InitializeComponent();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            FrmMenuPrincipal frm = new FrmMenuPrincipal();
            frm.Show();
            this.Close();
        }

        //linhas utilizadas para para apresentar as demais empresas do thui Tapioca
        private void CmbEmpresa_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nome FROM empresa";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nome";
            CmbEmpresa.ItemsSource = dt.DefaultView;

        }//FIM EMPRESA LOADED

        //linhas utilizadas para para apresentar as categorias disponiveis no cardapio do thui Tapioca
        private void CmbCategoria_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idCategoria, nomeCat FROM Categoria";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbCategoria.DisplayMemberPath = "nomeCat";
            CmbCategoria.ItemsSource = dt.DefaultView;
        }//FIM DA CATEGORIA


        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = false;
            CmbCategoria.IsEnabled = true;
            CmbCategoria.Focus();

        }

        //Linhas destinas a escolha do usuario no cadastramento de um novo item no cardápio
        private void CmbCategoria_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM categoria WHERE nomeCat = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbCategoria.Text;

                comm.CommandType = CommandType.Text;

                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigoCat = dr.GetString(0);

            }
            if (e.Key == Key.Enter)
            {
                BtnLimpar.IsEnabled = true;

                CmbEmpresa.IsEnabled = true;
                CmbEmpresa.Focus();
            }

        }// FIM escolha de categoria

        //Linhas destinadas a escolha da empresa thui tapioca que disponibilizará o item no seu cardápio
        private void CmbEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM empresa WHERE nome = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbEmpresa.Text;

                comm.CommandType = CommandType.Text;

                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigoEmp = dr.GetString(0);
            }
            if(e.Key == Key.Enter)
            {
                TxtNome.IsEnabled = true;
                TxtNome.Focus();

            }
        
            

        }//FIM escolha de empresa

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter) {
                TxtDescricao.IsEnabled = true;
                TxtDescricao.Focus();
            }
        }

        private void TxtDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }

            if(e.Key == Key.Enter){
                TxtValor.IsEnabled = true;
                TxtValor.Focus();

            }
        }

        private void TxtValor_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if(e.Key == Key.Enter){
                MessageBox.Show("Não Esqueça do status do item do cardápio!");
                ChkStatusCardapio.IsEnabled = true;
                TxtDataCad.Text = DateTime.Now.ToShortDateString();
                BtnSalvar.IsEnabled = true;

            }

        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime dataCadastro = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatusCardapio.IsChecked == true)
            {
                status = "ATIVO";

                string inserir = "INSERT INTO cardapio(idEmpresa,idCategoria,nomeProduto,descricao,valor,dataCadCardapio,statusCardapio)VALUES('"
                    + codigoEmp + "','"
                    + codigoCat + "','"
                    + TxtNome.Text + "','"
                    + TxtDescricao.Text + "','"
                    + TxtValor.Text + "','"
                    + dataCadastro.ToString("yyyy-MM-dd") + "','"
                    + status + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }

            else
            {
                status = "INATIVO";

                string inserir = "INSERT INTO cardapio(idEmpresa,idCategoria,nomeProduto,descricao,valor,dataCadCardapio,statusCardapio)VALUES('"
                    + codigoEmp + "','"
                    + codigoCat + "','"
                    + TxtNome.Text + "','"
                    + TxtDescricao.Text + "','"
                    + TxtValor.Text + "','"
                    + dataCadastro.ToString("yyyy-MM-dd") + "','"
                    + status + "')";
                
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }

            bd.Desconectar();
            MessageBox.Show("Novo item no cardápio cadastrado com sucesso!!", "Cadastro no cardápio");

            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            BtnSalvar.IsEnabled = false;

            CmbCategoria.IsEnabled = false;
            CmbEmpresa.IsEnabled = false;
            TxtNome.IsEnabled = false;
            TxtDescricao.IsEnabled = false;
            TxtDataCad.IsEnabled = false;
            ChkStatusCardapio.IsEnabled = false;

            CmbEmpresa.SelectedIndex = -1;
            CmbCategoria.SelectedIndex = -1;
            TxtNome.Clear();
            TxtDescricao.Clear();
            TxtValor.Clear();
            TxtDataCad.Clear();
            ChkStatusCardapio.IsChecked = false;
          
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {//limpar
            CmbEmpresa.SelectedIndex = -1;
            CmbCategoria.SelectedIndex = -1;
            TxtNome.Clear();
            TxtDescricao.Clear();
            TxtValor.Clear();
            TxtDataCad.Clear();
            ChkStatusCardapio.IsChecked = false;
            //FIM DA LIMPEZA
            //desabilitando os botões
            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;
            CmbEmpresa.IsEnabled = false;
            CmbCategoria.IsEnabled = false;
            TxtNome.IsEnabled = false;
            TxtDescricao.IsEnabled = false;
            TxtValor.IsEnabled = false;
            TxtDataCad.IsEnabled = false;
            ChkStatusCardapio.IsEnabled = false;

            BtnLimpar.IsEnabled = true;
            //Unico botão ativo.
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();

        }

    }
}
