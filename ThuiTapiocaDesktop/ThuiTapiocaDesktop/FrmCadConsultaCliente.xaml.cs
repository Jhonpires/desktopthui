﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
	/// <summary>
	/// Lógica interna para FrmCadConsultaCliente.xaml
	/// </summary>
	public partial class FrmCadConsultaCliente : Window
	{
        string codigo;

        public FrmCadConsultaCliente()
		{
			InitializeComponent();
			
		}
        //linhas utilizadas para para aprensentar as demais empresas do thui Tapioca
        private void FrmCadConsultaCliente1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nome FROM empresa";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nome";
            CmbEmpresa.ItemsSource = dt.DefaultView;
        }
        // essas linhas tem a função de atualizar o registro do cliente
        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            
            DateTime dataAtt = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();

            if (ChkStatusCli.IsChecked == true)
            {
                status = "ATIVO";//comando sql para atualizar, ainda em fase teste.
                string atualizar = "UPDATE cliente SET idEmpresa='" + CmbEmpresa.Text +
                    "', nome='" + TxtNome.Text +
                    "', cpf='" + TxtCpf.Text +
                    "', telefone='" + TxtTelefone.Text +
                    "', celular= '" + TxtCelular.Text +
                    "', email='" + TxtEmail.Text +
                    "', endereco='" + TxtEndereco.Text +
                    "', uf='" + TxtUf.Text +
                    "', cep='" + TxtCep.Text +
                    "', login='" + TxtLogin.Text +
                    "', senha='" + TxtSenha.Text +
                    "', dataCadCli='" + dataAtt.ToString("yyyy-MM-dd") +
                    "', statusCli='" + status +
                    "'WHERE idCliente='" + TxtCliente.Text + "'";
                MySqlCommand comandos = new MySqlCommand(atualizar, bd.conexao);
                comandos.ExecuteNonQuery();
                MessageBox.Show("Cadastro de cliente atualizado sucesso!!", "Atualização de cadastro de Cliente");

            }
            //else
            //{
            //    status = "INATIVO";
            //    string atualizar = "UPDATE cliente SET idEmpresa='" + CmbEmpresa.Text + "','" + "nome=" + TxtNome.Text + "','" + "cpf= " + TxtCpf.Text + "','" + "telefone=" + TxtTelefone.Text + "','" + "celular=" + TxtCelular.Text + "','" + "email=" + TxtEmail.Text + "','" + "endereco=" + TxtEndereco.Text + "','" + "uf=" + TxtUf.Text + "','" + "cep=" + TxtCep.Text + "','" + "login=" + TxtLogin.Text + "','" + "senha=" + TxtSenha.Text + "','" + "dataCadCli=" + dataAtt.ToString("yyyy-MM-dd") + "','" + "statusCli=" + status + "'WHERE idCliente'" + TxtCliente.Text + "'";
            //    MySqlCommand comandos = new MySqlCommand(atualizar, bd.conexao);
            //    comandos.ExecuteNonQuery();
            //    MessageBox.Show("Cadastro de cliente atualizado sucesso!!", "Atualização de cadastro de Cliente");

            //}

        }//FIM DA ATUALIZAÇÃO




    }
}
