﻿#pragma checksum "..\..\FrmCadEmpresa.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "165CFCA0A3406F53AAE45A051D0C200A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ThuiTapiocaDesktop;


namespace ThuiTapiocaDesktop {
    
    
    /// <summary>
    /// FrmCadEmpresa
    /// </summary>
    public partial class FrmCadEmpresa : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ThuiTapiocaDesktop.FrmCadEmpresa FrmCadEmpresa;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSalvar;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnNovo;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnLimpar;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSair;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCadastroCliente;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtNome;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtNumero;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtComplemento;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBairro;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCidade;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtUf;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCep;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtDataAbertura;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtDataFim;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtEndereco;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblNome;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblEndereco;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblNumero;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblComplemento;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblBairro;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCidade;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LbllUf;
        
        #line default
        #line hidden
        
        
        #line 195 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCep;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDataAbertura;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDataFim;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtTelefone1;
        
        #line default
        #line hidden
        
        
        #line 233 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtEmail;
        
        #line default
        #line hidden
        
        
        #line 241 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblTelefone1;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblEmail;
        
        #line default
        #line hidden
        
        
        #line 261 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtTelefone2;
        
        #line default
        #line hidden
        
        
        #line 269 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblTelefone2;
        
        #line default
        #line hidden
        
        
        #line 279 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtDataCadCliente;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\FrmCadEmpresa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDatCadCliente;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ThuiTapiocaDesktop;component/frmcadempresa.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FrmCadEmpresa.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FrmCadEmpresa = ((ThuiTapiocaDesktop.FrmCadEmpresa)(target));
            return;
            case 2:
            this.BtnSalvar = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.BtnNovo = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.BtnLimpar = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.BtnSair = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.LblCadastroCliente = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.TxtNome = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.TxtNumero = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TxtComplemento = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.TxtBairro = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.TxtCidade = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.TxtUf = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.TxtCep = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.TxtDataAbertura = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.TxtDataFim = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.TxtEndereco = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.LblNome = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.LblEndereco = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.LblNumero = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.LblComplemento = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.LblBairro = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.LblCidade = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.LbllUf = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.LblCep = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.LblDataAbertura = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.LblDataFim = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.TxtTelefone1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.TxtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.LblTelefone1 = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.LblEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.TxtTelefone2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.LblTelefone2 = ((System.Windows.Controls.Label)(target));
            return;
            case 33:
            this.TxtDataCadCliente = ((System.Windows.Controls.TextBox)(target));
            
            #line 279 "..\..\FrmCadEmpresa.xaml"
            this.TxtDataCadCliente.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtNome_KeyDown);
            
            #line default
            #line hidden
            return;
            case 34:
            this.LblDatCadCliente = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

