﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
	/// <summary>
	/// Lógica interna para FrmTodosClientes.xaml
	/// </summary>
	public partial class FrmTodosClientes : Window
	{
		string nome;
		string status;
		string empresa;


		public FrmTodosClientes()
		{
			InitializeComponent();
		}

        private void BtnVoltar_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            FrmCadCliente frm = new FrmCadCliente();
            frm.Show();
            this.Close();
        }

      
        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
			//dATAGRID, que tem a função de habilitar em suas respectivas datasrid os inativos e ativos.
			//Nessa caso está sendo tratado o ativo
            if (DtgAtivo.SelectedIndex >= 0)//Habilitando uma condição que SE for mais que 0 deve fazer:
            {
				//Var que recebe o que foi clicado na dataGrid
                var exibeAtivo = DtgAtivo.SelectedItems[0] as DataRowView;
                FrmCadConsultaCliente frm = new FrmCadConsultaCliente();
                frm.Show();
                nome = exibeAtivo["nome"].ToString();
				//Tranferindo o que foi clicado para uma string que tem a função de receber o campo "nome" como declarado no banco

                Banco bd = new Banco();
                bd.Conectar();
				//Dando o Select onde o nome(campo do banco) será colocado na string nome.
                string query = "SELECT * FROM cliente WHERE nome = '" + nome + "'";
                MySqlCommand cmd = new MySqlCommand(query, bd.conexao);

                cmd.CommandType = CommandType.Text;

                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
				//TxtCliente.Text será o responsavel por exibir o idCliente de forma que o usuario não pode alterar.
				//Essas linhas tambem tem a função de colocar o que for puxado do banco nos respectivos campos
                frm.TxtCliente.Text = dr.GetString(0);
                empresa = dr.GetString(1);

                frm.TxtNome.Text = dr.GetString(2);
                frm.TxtCpf.Text = dr.GetString(3);
                frm.TxtTelefone.Text = dr.GetString(4);
                frm.TxtCelular.Text = dr.GetString(5);
                frm.TxtEmail.Text = dr.GetString(6);
                frm.TxtEndereco.Text = dr.GetString(7);
                frm.TxtUf.Text = dr.GetString(8);
                frm.TxtCep.Text = dr.GetString(9);
                frm.TxtLogin.Text = dr.GetString(10);
                frm.TxtSenha.Text = dr.GetString(11);
                frm.TxtDataCadCliente.Text = dr.GetString(12);
                status = dr.GetString(13);
                if (status == "ATIVO")
                {
                    frm.ChkStatusCli.IsChecked = true;
                }

                bd.Desconectar();

                Banco bd1 = new Banco();
                bd1.Conectar();
				//Selecionando o id da empresa que o cliente está sendo cadastrado
				//Será possivel alterar no FrmCadConsultaCliente
                string selecionarempresa = "SELECT * FROM empresa WHERE idEmpresa = '" + empresa + "'";
                MySqlCommand cmd1 = new MySqlCommand(selecionarempresa, bd1.conexao);

                cmd1.CommandType = CommandType.Text;
                MySqlDataReader dr1 = cmd1.ExecuteReader();
                dr1.Read();
                frm.CmbEmpresa.Text = dr1.GetString(1);


            }
            else if (DtgInativo.SelectedIndex >= 0)
            {
                var exibeInativo = DtgInativo.SelectedItems[0] as DataRowView;
                FrmCadConsultaCliente frm = new FrmCadConsultaCliente();
                frm.Show();
                nome = exibeInativo["nome"].ToString();
                
                Banco bd = new Banco();
                bd.Conectar();

                string query = "SELECT * FROM cliente WHERE nome = '" + nome + "'";
                MySqlCommand cmd = new MySqlCommand(query, bd.conexao);

                cmd.CommandType = CommandType.Text;

                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Read();

                frm.TxtCliente.Text = dr.GetString(0);
                empresa = dr.GetString(1);

                frm.TxtNome.Text = dr.GetString(2);
                frm.TxtCpf.Text = dr.GetString(3);
                frm.TxtTelefone.Text = dr.GetString(4);
                frm.TxtCelular.Text = dr.GetString(5);
                frm.TxtEmail.Text = dr.GetString(6);
                frm.TxtEndereco.Text = dr.GetString(7);
                frm.TxtUf.Text = dr.GetString(8);
                frm.TxtCep.Text = dr.GetString(9);
                frm.TxtLogin.Text = dr.GetString(10);
                frm.TxtSenha.Text = dr.GetString(11);
                frm.TxtDataCadCliente.Text = dr.GetString(12);
                status = dr.GetString(13);
                if (status == "INATIVO")
                {
                    frm.ChkStatusCli.IsChecked = false;
                }

                bd.Desconectar();

                Banco bd1 = new Banco();
                bd1.Conectar();

                string selecionarempresa = "SELECT * FROM empresa WHERE idEmpresa = '" + empresa + "'";
                MySqlCommand cmd1 = new MySqlCommand(selecionarempresa, bd1.conexao);

                cmd1.CommandType = CommandType.Text;
                MySqlDataReader dr1 = cmd1.ExecuteReader();
                dr1.Read();
                frm.CmbEmpresa.Text = dr1.GetString(1);

            }

		}

		private void FrmTodosClientes1_Loaded(object sender, RoutedEventArgs e)
		{
			Banco bd = new Banco();
			bd.Conectar();

			string selecionar = "SELECT nome FROM cliente where statusCli = 'ATIVO'";
			MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

			MySqlDataAdapter da = new MySqlDataAdapter(com);
			DataTable dt = new DataTable();
			da.Fill(dt);
			DtgAtivo.DisplayMemberPath = "nome";
			DtgAtivo.ItemsSource = dt.DefaultView;
			DtgAtivo.Columns[0].Header="Nome";
			bd.Desconectar();
		
			Banco bd1 = new Banco();
			bd1.Conectar();

			string selecionar1 = "SELECT nome FROM cliente where statusCli = 'INATIVO'";
			MySqlCommand com1 = new MySqlCommand(selecionar1, bd1.conexao);

			MySqlDataAdapter da1 = new MySqlDataAdapter(com1);
			DataTable dt1 = new DataTable();
			da1.Fill(dt1);
			DtgInativo.DisplayMemberPath = "nome";
			DtgInativo.ItemsSource = dt1.DefaultView;
			bd1.Desconectar();
		}
	}
}

