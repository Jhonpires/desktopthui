﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
	/// <summary>
	/// Lógica interna para FrmCadPedido.xaml
	/// </summary>
	public partial class FrmCadPedido : Window
	{
		string codigoCli, codigoFunc, codigoCar;
		public FrmCadPedido()
		{
			InitializeComponent();
		}

		private void CmbCliente_Loaded(object sender, RoutedEventArgs e)
		{
			Banco bd = new Banco();
			bd.Conectar();

			string selecionar = "SELECT idCliente, nome FROM cliente";
			MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

			MySqlDataAdapter da = new MySqlDataAdapter(com);
			DataTable dt = new DataTable();
			da.Fill(dt);
			CmbCliente.DisplayMemberPath = "nome";
			CmbCliente.ItemsSource = dt.DefaultView;

		}

		private void CmbFuncionario_Loaded(object sender, RoutedEventArgs e)
		{
			Banco bd = new Banco();
			bd.Conectar();

			string selecionar = "SELECT idFuncionario, nome FROM funcionario";
			MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

			MySqlDataAdapter da = new MySqlDataAdapter(com);
			DataTable dt = new DataTable();
			da.Fill(dt);
			CmbFuncionario.DisplayMemberPath = "nome";
			CmbFuncionario.ItemsSource = dt.DefaultView;
		}



		private void CmbCardapio_Loaded(object sender, RoutedEventArgs e)
		{
			Banco bd = new Banco();
			bd.Conectar();

			string selecionar = "SELECT idCardapio, nome FROM cardapio";
			MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

			MySqlDataAdapter da = new MySqlDataAdapter(com);
			DataTable dt = new DataTable();
			da.Fill(dt);
			CmbCardapio.DisplayMemberPath = "nome";
			CmbCardapio.ItemsSource = dt.DefaultView;
		}

		private void CmbCliente_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				Banco bd = new Banco();
				bd.Conectar();
				MySqlCommand comm = new MySqlCommand("SELECT * FROM cliente WHERE nome = ?", bd.conexao);
				comm.Parameters.Clear();
				comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbCliente.Text;

				comm.CommandType = CommandType.Text;

				MySqlDataReader dr = comm.ExecuteReader();
				dr.Read();

				codigoCli = dr.GetString(0);

			}
			if (e.Key == Key.Enter)
			{
				BtnLimpar.IsEnabled = true;

				CmbFuncionario.IsEnabled = true;
				CmbFuncionario.Focus();
			}
		}

		private void CmbFuncionario_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				Banco bd = new Banco();
				bd.Conectar();
				MySqlCommand comm = new MySqlCommand("SELECT * FROM funcionario WHERE nome = ?", bd.conexao);
				comm.Parameters.Clear();
				comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbFuncionario.Text;

				comm.CommandType = CommandType.Text;

				MySqlDataReader dr = comm.ExecuteReader();
				dr.Read();

				codigoFunc = dr.GetString(0);

			}
			if (e.Key == Key.Enter)
			{
				BtnLimpar.IsEnabled = true;

				CmbCardapio.IsEnabled = true;
				CmbCardapio.Focus();
			}
		}

		private void CmbCardapio_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				Banco bd = new Banco();
				bd.Conectar();
				MySqlCommand comm = new MySqlCommand("SELECT * FROM cardapio WHERE nome = ?", bd.conexao);
				comm.Parameters.Clear();
				comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbCardapio.Text;

				comm.CommandType = CommandType.Text;

				MySqlDataReader dr = comm.ExecuteReader();
				dr.Read();

				codigoCar = dr.GetString(0);

			}
			if (e.Key == Key.Enter)
			{
				BtnLimpar.IsEnabled = true;

				CmbTipoPagamento.IsEnabled = true;
				CmbTipoPagamento.Focus();
			}
		}

		private void CmbTipoPagamento_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			TxtDataCadPedido.IsEnabled = true;
			TxtDataCadPedido.Focus();

		}

		private void TxtDataCadPedido_KeyDown(object sender, KeyEventArgs e)
		{
			TxtDataCadPedido.MaxHeight = 10;
			KeyConverter key = new KeyConverter();
			if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
				&& (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
				&& (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

			{
				MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

				e.Handled = true;
			}//somente numeros e pontuação 

			if (e.Key == Key.Enter)
			{

				TxtHorarioPedido.IsEnabled = true;
				TxtHorarioPedido.Focus();
			}
		}

		private void TxtHorarioPedido_KeyDown(object sender, KeyEventArgs e)
		{
			TxtHorarioPedido.MaxHeight = 5;
			KeyConverter key = new KeyConverter();
			if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
				&& (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
				&& (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

			{
				MessageBox.Show("Este campo somente aceita números e ':' no formato> 8:20 ou 08:20!");

				e.Handled = true;
			}//somente numeros e pontuação 

			if (e.Key == Key.Enter)
			{

				TxtValorTotal.IsEnabled = true;
				TxtValorTotal.Focus();
			}
		}

		private void TxtValorTotal_KeyDown(object sender, KeyEventArgs e)
		{//Ainda falta o simbolo especifico para dinheiro ou irei achar uma forma de habilitar ele na textbox, talves mudar para float.
			
			KeyConverter key = new KeyConverter();
			if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)&&(e.Key!= Key.LeftShift)
				&& (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
				&& (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

			{
				MessageBox.Show("Este campo somente aceita números referentes a valor");

				e.Handled = true;
			}//somente numeros e pontuação 

			if (e.Key == Key.Enter)
			{
				BtnLimpar.IsEnabled = true;
				BtnSalvar.IsEnabled = true;
				BtnSalvar.Focus();
			}
		}

		
	}
}
